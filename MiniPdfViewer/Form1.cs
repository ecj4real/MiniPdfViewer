﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniPdfViewer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void openPDFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //openFileDialog.Filter = "PDF Files|*.pdf";
            //openFileDialog.ShowDialog();
            //if(openFileDialog != null)
            //{
            //    path = openFileDialog.FileName;
            //    path.Replace('\\', '/');

            //    string text = string.Empty;
            //    PdfReader reader = new PdfReader(path);
            //    for (int page = 1; page <= reader.NumberOfPages; page++)
            //    {
            //        text += PdfTextExtractor.GetTextFromPage(reader, page);
            //    }
            //    reader.Close();

            //    richTextBox.Text = text;
            //    panel.Visible = true;
            //    richTextBox.Visible = true;           
            //}
            openFileDialog.Filter = "PDF Files|*.pdf";
            openFileDialog.ShowDialog();
            if(openFileDialog != null)
            {
                string path = openFileDialog.FileName;
                path.Replace('\\', '/');
                try
                {
                    
                    axAcroPDF1.LoadFile(path);
                    axAcroPDF1.src = @path;
                    axAcroPDF1.setView("FitH");
                    axAcroPDF1.setLayoutMode("SinglePage");
                    axAcroPDF1.setShowToolbar(false);
                    axAcroPDF1.Show();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

        }

        private void ViewerClosed(object sender, FormClosedEventArgs e)
        {
            axAcroPDF1.Dispose();
            axAcroPDF1 = null;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            axAcroPDF1.Hide();
            this.WindowState = FormWindowState.Maximized;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            axAcroPDF1.Hide();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox1 about = new AboutBox1();
            about.StartPosition = FormStartPosition.CenterScreen;
            about.Show();
        }
    }
}
